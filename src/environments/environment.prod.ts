export const environment = {
  production: true,
  apiUrl: 'https://contacts-manager-api.now.sh/'
};
