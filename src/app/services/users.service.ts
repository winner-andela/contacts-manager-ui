/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {environment} from '../../environments/environment';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient) {}

  createUser(user: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}users`, user);
  }

  createAdmin(admin: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}users/admin`, admin);
  }

  retrieveUsers(): Observable<Array<any>> {
    return this.http.get<Array<any>>(`${environment.apiUrl}users`);
  }

  retrieveAdmins(): Observable<Array<any>> {
    return this.http.get<Array<any>>(`${environment.apiUrl}users/admin`);
  }
}
