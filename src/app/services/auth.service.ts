/**
 * Created by bolorundurowb on 9/28/2018
 */
import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';

@Injectable()
export class AuthService {
  userKey = 'cm-user';
  userTokenKey = 'cm-token';

  constructor(private http: HttpClient) {}

  login(payload: {username: string, password: string}): Observable<any> {
    return this.http.post(`${environment.apiUrl}login`, payload);
  }

  saveUser(user: any): void {
    localStorage.setItem(this.userKey, JSON.stringify(user));
  }

  saveToken(token: string): void {
    localStorage.setItem(this.userTokenKey, token);
  }

  retrieveUser(): any {
    const retrievedObject = localStorage.getItem(this.userKey);
    return JSON.parse(retrievedObject);
  }

  retrieveToken(): string {
    return localStorage.getItem(this.userTokenKey);
  }

  clearData(): void {
    localStorage.removeItem(this.userKey);
    localStorage.removeItem(this.userTokenKey);
  }

  isLoggedIn(): boolean {
    if (this.retrieveToken()) {
      return true;
    }
    return false;
  }

  logout(): void {
    this.clearData();
    window.location.href = '/';
  }
}
