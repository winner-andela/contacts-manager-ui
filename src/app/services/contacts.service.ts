/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {environment} from '../../environments/environment';

@Injectable()
export class ContactsService {
  constructor(private http: HttpClient) {}

  getAllContacts(query: string = ''): Observable<Array<any>> {
    return this.http.get<Array<any>>(`${environment.apiUrl}contacts?search=${query}`);
  }

  addContact(contact: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}contacts`, contact);
  }

  sendSms(contactId: string, content: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}contacts/${contactId}/sms`, content);
  }

  sendEmail(contactId: string, content: any): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}contacts/${contactId}/email`, content);
  }
}
