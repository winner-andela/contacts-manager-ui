/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';

@Injectable()
export class AdminGuard implements  CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  isSuper() {
    if (!this.authService.isLoggedIn()) {
      return false;
    }

    const user = this.authService.retrieveUser();
    if (!user) {
      return false;
    }

    return user.role === 'admin';
  }

  canActivate() {
    return this.isSuper();
  }
}
