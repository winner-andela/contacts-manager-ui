/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Component} from '@angular/core';
import {AuthService} from '../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  isAdmin: boolean;
  isSuper: boolean;

  currentUser: string;

  constructor(private auth: AuthService) {
    const user = auth.retrieveUser();

    if (!user) {
      return;
    }

    this.currentUser = user.username;

    if (user.role === 'admin') {
      this.isAdmin = true;
    }
    if (user.role === 'super') {
      this.isSuper = true;
    }
  }

  logout(): void {
    this.auth.clearData();
    window.location.href = '/';
  }
}
