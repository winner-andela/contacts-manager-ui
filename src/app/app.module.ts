import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {AuthService} from './services/auth.service';
import {JwtInterceptor} from './interceptors/jwt.interceptor';
import {LoginComponent} from './pages/login/login.component';
import {AlertModule} from 'ngx-bootstrap/alert';
import {TenantComponent} from './pages/tenant/tenant.component';
import {AuthGuard} from './guards/auth.guard';
import {SuperGuard} from './guards/super.guard';
import {NavbarComponent} from './navbar/navbar.component';
import {ContactsComponent} from './pages/contacts/contacts.component';
import {UsersComponent} from './pages/users/users.component';
import {AdminGuard} from './guards/admin.guard';
import {ModalModule} from 'ngx-bootstrap/modal';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'tenants',
    component: TenantComponent,
    canActivate: [AuthGuard, SuperGuard]
  },
  {
    path: 'contacts',
    component: ContactsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users',
    component: UsersComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: '**',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TenantComponent,
    NavbarComponent,
    ContactsComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes, {enableTracing: false}),
    AlertModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [
    AuthService,
    AuthGuard,
    AdminGuard,
    SuperGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
