/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Component, OnInit, TemplateRef} from '@angular/core';
import {UsersService} from '../../services/users.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-tenant',
  templateUrl: './tenant.component.html',
  styleUrls: ['./tenant.component.scss'],
  providers: [UsersService]
})
export class TenantComponent implements OnInit {
  admins: Array<any> = [];
  modalRef: BsModalRef;
  admin: any = {};
  isCreatingAdmin: boolean;

  hasModalError: boolean;
  modalErrorMessage: string;

  constructor(private usersService: UsersService, private modalService: BsModalService) {}

  ngOnInit(): void {
    this.usersService.retrieveAdmins()
      .subscribe((res) => {
        this.admins = res;
      });
  }

  createAdmin(): void {
    this.isCreatingAdmin = true;
    this.hasModalError = false;

    this.usersService.createAdmin(this.admin)
      .subscribe((res) => {
        this.isCreatingAdmin = false;
        this.admins.push(res);

        this.admin = {};

        this.modalRef.hide();
      }, (err) => {
        this.isCreatingAdmin = false;
        this.hasModalError = true;
        this.modalErrorMessage = err.error.message || err.message;
      });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
