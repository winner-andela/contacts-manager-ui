/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Component, OnInit, TemplateRef} from '@angular/core';
import {ContactsService} from '../../services/contacts.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
  providers: [ContactsService]
})
export class ContactsComponent implements OnInit {
  isUser: boolean;

  contacts: Array<any> = [];

  modalRef: BsModalRef;
  contact: any = {};
  sms: any = {};
  email: any = {};

  hasModalError: boolean;
  modalErrorMessage: string;

  hasModalSuccess: boolean;
  modalSuccessMessage: string;

  isAddingContact: boolean;
  isSendingSms: boolean;
  isSendingEmails: boolean;

  selectedContactId: any;

  constructor(private contactsService: ContactsService, private modalService: BsModalService, auth: AuthService) {
    const user = auth.retrieveUser();

    if (!user) {
      auth.logout();
    } else {
      this.isUser = user.role === 'user';
    }
  }

  ngOnInit(): void {
    this.contactsService.getAllContacts()
      .subscribe((res) => {
        this.contacts = res;
      });
  }

  openModal(template: TemplateRef<any>, contactId?) {
    this.modalRef = this.modalService.show(template);
    this.selectedContactId = contactId;
  }

  addContact(): void {
    this.isAddingContact = true;
    this.hasModalError = false;

    this.contactsService.addContact(this.contact)
      .subscribe((res) => {
        this.isAddingContact = false;
        this.contacts.push(res);

        this.contact = {};

        this.modalRef.hide();
      }, (err) => {
        this.isAddingContact = false;
        this.hasModalError = true;
        this.modalErrorMessage = err.error.message || err.message;
      });
  }

  sendSms(): void {
    this.isSendingSms = true;
    this.hasModalError = false;

    this.contactsService.sendSms(this.selectedContactId, this.sms)
      .subscribe((res) => {
        this.isSendingSms = false;

        this.modalSuccessMessage = res.message;
        this.hasModalSuccess = true;

        this.sms = {};
      }, (err) => {
        this.isSendingSms = false;
        this.hasModalError = true;
        this.modalErrorMessage = err.error.message || err.message;
      });
  }

  sendEmail(): void {
    this.isSendingEmails = true;
    this.hasModalError = false;
    this.hasModalSuccess = false;

    this.contactsService.sendEmail(this.selectedContactId, this.email)
      .subscribe((res) => {
        this.isSendingEmails = false;

        this.modalSuccessMessage = res.message;
        this.hasModalSuccess = true;

        this.email = {};
      }, (err) => {
        this.isSendingEmails = false;
        this.hasModalError = true;
        this.modalErrorMessage = err.error.message || err.message;
      });
  }

  search(event: any): void {
    const search = event.target.value;
    this.contactsService.getAllContacts(search)
      .subscribe((res) => {
        this.contacts = res;
      });
  }
}
