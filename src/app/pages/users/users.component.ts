/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Component, OnInit, TemplateRef} from '@angular/core';
import {UsersService} from '../../services/users.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UsersService]
})
export class UsersComponent implements OnInit {
  users: Array<any> = [];
  modalRef: BsModalRef;
  isCreatingUser: boolean;
  user: any = {};

  hasModalError: boolean;
  modalErrorMessage: string;

  constructor(private usersService: UsersService, private modalService: BsModalService) {}

  ngOnInit(): void {
    this.usersService.retrieveUsers()
      .subscribe((res) => {
        this.users = res;
      });
  }

  createUser(): void {
    this.isCreatingUser = true;
    this.hasModalError = false;

    this.usersService.createUser(this.user)
      .subscribe((res) => {
        this.isCreatingUser = false;
        this.users.push(res);

        this.user = {};

        this.modalRef.hide();
      }, (err) => {
        this.isCreatingUser = false;
        this.hasModalError = true;
        this.modalErrorMessage =  err.error.message || err.message;
      });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
