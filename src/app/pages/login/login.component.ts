/**
 * Created by bolorundurowb on 9/28/2018
 */

import {Component, OnInit, SecurityContext} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: any = {};
  alerts: Array<any> = [];

  constructor(private auth: AuthService, private sanitizer: DomSanitizer, private router: Router) {}

  ngOnInit(): void {
    if (this.auth.isLoggedIn()) {
      const user = this.auth.retrieveUser();
      if (user.role === 'super') {
        this.router.navigate(['tenants']);
      } else {
        this.router.navigate(['contacts']);
      }
    }
  }

  login(): void {
    this.alerts = [];
    this.auth.login(this.user)
      .subscribe((res) => {
        this.auth.saveUser(res.user);
        this.auth.saveToken(res.token);

        // redirect to the tenants page
        if (res.user.role === 'super') {
          window.location.href = '/tenants';
        } else {
          window.location.href = '/contacts';
        }
      }, (err) => {
        this.alerts = [{
          type: 'danger',
          msg: this.sanitizer.sanitize(SecurityContext.HTML, err.error.message || err.message)
        }];
      });
  }
}
